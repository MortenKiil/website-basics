function readFileContent(file) {
	const reader = new FileReader()
  return new Promise((resolve, reject) => {
    reader.onload = event => resolve(event.target.result)
    reader.onerror = error => reject(error)
    reader.readAsText(file)
  })
}

function printFileContent() {
  var file = document.getElementById("fileToLoad").files[0];
  var contentTarget = document.getElementById('content-target');
  var answerTarget = document.getElementById('answer-target');
	readFileContent(file).then(content => {
  	contentTarget.value = content;
    answerTarget.value = findMostFrequent(content);
  }).catch(error => console.log(error))
}

function findSorted(content) {
  var lines = content.split('\n');
  var parsedLines = [];
  for (var x = 0; x < lines.length; x++) {
    var parsed = parseInt(lines[x]);
    parsedLines.push(parsed);
  }
  var sortedLines = parsedLines.sort();
  return sortedLines;
}

function findMostFrequent (content) {
  var sortedLines = findSorted(content);
  var integerArray = []
  var frequencyArray = []

  for (var x = 0; x < sortedLines.length; x++) {
    var myInt = sortedLines[x];
    if (integerArray.includes(myInt)) {
      frequencyArray[integerArray.length - 1] = frequencyArray[integerArray.length - 1] + 1;
    } else {
      frequencyArray.push(1);
      integerArray.push(myInt);
    }
  }

  var copiedFrequency = [... frequencyArray];
  var sortedFrequency = copiedFrequency.sort(function(a, b) {
    return a - b;
  });

  var answerArray = []
  for (var y = sortedFrequency.length - 1 ; y > sortedFrequency.length - 6; y--) {
    var myFreq = sortedFrequency[y];
    if (myFreq) {
      var myIndex = frequencyArray.indexOf(myFreq);
      var answerInt = integerArray[myIndex];
      answerArray.push(answerInt);

      // removing frequeny and index to prevent problems with ties
      integerArray.splice(myIndex, 1);
      frequencyArray.splice(myIndex, 1);
    }
    
  }

  return answerArray;
}
